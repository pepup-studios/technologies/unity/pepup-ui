﻿using UnityEngine;
using TMPro;
using PepUp.Collections;
using System.Collections;

public class ReusableTextReceiver : MonoBehaviour {

    public TextMeshProUGUI textMesh;
    public Animator animator;
    public bool putObjectToPool = true;
    public bool autoClose;
    public float closeTime;

    public bool isClosed { get; private set; }

    protected IEnumerator enumerator;

    public void UpdateText(string text) {
        textMesh.text = text;
    }

    public void StartAnimation() {
        isClosed = false;
        animator.SetTrigger("Open");
        if (autoClose) {
            StopEnumerator();
            StartEnumerator();
        }
    }

    public void StopAnimation() {
        StopEnumerator();
        animator.SetTrigger("Close");
        isClosed = true;
    }

    protected void OnDisable() {
        StopEnumerator();
    }

    protected void StartEnumerator() {
        enumerator = CloseTime();
        StartCoroutine(enumerator);
    }

    protected void StopEnumerator() {
        if (enumerator != null) StopCoroutine(enumerator);
    }

    protected IEnumerator CloseTime() {
        yield return new WaitForSeconds(closeTime);
        StopAnimation();
    }

    public void PutObjectToPool() {
        if (!putObjectToPool) return;
        ObjectPool.intance.PutObject("MemorizeTimerText", gameObject.name, this);
    }
}
