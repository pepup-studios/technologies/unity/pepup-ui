﻿using PepUp;
using PepUp.Events;
using PepUp.Utility;
using UnityEngine;
using UnityEngine.UI;

public class SensitivityController : MonoBehaviour
{
    public Vector2 defaultSensitivity = new Vector2(0.035f, 0.06f);
    public bool loadOnAwake;
    public Vector2Object sensitivityObj;
    public Slider slider; 
    //public RangedFloatOption clamp;
    //public RangedFloatOption remapClamp;
    public RegisterEvent DoSave;

    protected float currSensitivity;

    public float SetSensitivity {
        set { sensitivityObj.value = defaultSensitivity * (currSensitivity = value); }
    }

    //public float SetSensitivityRemap {
    //    set {
    //        float percentage = (value - remapClamp.value.minValue) / (remapClamp.value.maxValue - remapClamp.value.minValue);
    //        sensitivityObj.value = clamp.value.minValue + (percentage * (clamp.value.maxValue - clamp.value.minValue));
    //    }
    //}

    private void Awake()
    {
        DoSave.Register(this,"Save");
        if (!loadOnAwake) return;
        LoadSensitivity();
        UpdateSlider();
    }

    private void OnDestroy()
    {
        DoSave.Unregister();
    }

    public void Save(params object[] objs) {
        SaveSensitivity();
    }

    public void LoadSensitivity() {
        if (SaveUtility.LoadFromBinaryFormatter("sensitivityInfo", out currSensitivity))
        {
            sensitivityObj.value = defaultSensitivity * currSensitivity;
        }
        else {
            currSensitivity = 1;
            sensitivityObj.value = defaultSensitivity;
        }
    }

    protected void UpdateSlider() {
        slider.value = currSensitivity;
    }

    public void SaveSensitivity() {
        SaveUtility.SaveAsBinaryFormatter(currSensitivity, "sensitivityInfo");
    }
}
