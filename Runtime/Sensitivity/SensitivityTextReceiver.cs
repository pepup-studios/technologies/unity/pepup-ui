﻿using PepUp.Events;
using TMPro;
using UnityEngine;

public class SensitivityTextReceiver : MonoBehaviour
{
    public string prefix;
    public string suffix;
    public uint decimalPoint = 0;
    //public Text textMesh;
    public TextMeshProUGUI textMeshPro;

    public RegisterEvent OnUpdateText;

    public float UpdateTextByFloat {
        set { UpdateText(value); }
    }

    private void OnEnable()
    {
        OnUpdateText.Register(this, "UpdateText");
    }

    private void OnDisable()
    {
        OnUpdateText.Unregister();
    }

    protected void UpdateText(params object[] objs) {
        float value = (float)objs[1];
        UpdateText(value.ToString());
    }

    public void UpdateText(float text) {
        textMeshPro.text = text.ToString("F" + decimalPoint) + suffix;
    }

    public void UpdateText(string text)
    {
        textMeshPro.text = text + suffix;
    }
}
