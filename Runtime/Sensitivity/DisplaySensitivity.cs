﻿using PepUp;
using PepUp.Events;
using UnityEngine;
using UnityEngine.UI;

public class DisplaySensitivity : MonoBehaviour
{
    public Slider sensitivitySlider;
    public FloatObject sensitivityObj;
    public RangedFloatOption clamp;
    public RangedFloatOption remapClamp;
    public RegisterEvent DoUpdateSlider;

    private void Awake()
    {
        DoUpdateSlider.Register(this, "UpdateSlider");
        UpdateSlider();
    }

    private void OnDestroy()
    {
        DoUpdateSlider.Unregister();
    }

    public void UpdateSlider(params object[] objs) {
        float percentage = (sensitivityObj.value - clamp.value.minValue) / (clamp.value.maxValue - clamp.value.minValue);
        float value = remapClamp.value.minValue + (percentage * (remapClamp.value.maxValue - remapClamp.value.minValue));

        sensitivitySlider.value = value;
    }
}
