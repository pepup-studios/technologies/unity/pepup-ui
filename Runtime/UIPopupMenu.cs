﻿using PepUp.Events;

namespace PepUp.UI {
    [UnityEngine.AddComponentMenu("PepUp/UI/Popup Menu")]
    public class UIPopupMenu : Menu {

        public EventObject OnOpen;
        public EventObject OnClose;

        public event EventAction OnOpenEvent;
        public event EventAction OnCloseEvent;

        public override MenuTypes GetMenuType() {
            return MenuTypes.Popup;
        }

        public override void Open(Menu parent) {
            if (isActive) return;

            base.Open(parent);
            OnOpen?.Invoke(gameObject.name);
            OnOpenEvent?.Invoke(gameObject.name);
        }

        public override Menu OpenNew(Menu parent) {
            Close();
            if (this.parent != null && this.parent != parent && parent as UIPopupMenu == null)
            {
                this.parent.Close();
            }
            parent.Open(this.parent);
            return parent;
        }

        public override void Close() {
            base.Close();
            OnClose?.Invoke(gameObject.name);
            OnCloseEvent?.Invoke(gameObject.name);
        }
    }
}