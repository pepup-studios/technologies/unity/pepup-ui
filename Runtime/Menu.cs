﻿using UnityEngine;

namespace PepUp.UI {
    [RequireComponent(typeof(Animator))]
    public abstract class Menu : MonoBehaviour {

        public string MenuName { get { return gameObject.name; } }

        public TransitionTypes defaultTransition = TransitionTypes.Default;
        [Space]
        public Menu header;
        //public TransitionTypes headerTransition;

        [SerializeField]
        [HideInInspector]
        private Animator _animator;
        public Animator Animator { get { return _animator; } private set { _animator = value; } }

        protected Menu parent;
        protected string openParameter = "";
        protected string closedParameter = "";

        protected bool isActive;

        public abstract MenuTypes GetMenuType();

        public virtual void Open(Menu parent) {
            Activate(true);
            this.parent = parent;

            if (parent != null && parent.header != null) {
                parent.header.Close();
            }
        }

        public abstract Menu OpenNew(Menu parent);

        public virtual Menu SelfClose()
        {
            Close();
            return parent;
        }

        public virtual void Close() {
            Activate(false);
        }

        public virtual void Activate(bool value) {
            isActive = value;
            if (value) {
                gameObject.SetActive(value);
                _animator.SetTrigger(openParameter);
                if (header != null) {
                    OpenNew(header);
                }
            } else {
                _animator.SetTrigger(closedParameter);
            }
        }

        public void InitParameter() {
            SetParameter(defaultTransition.ToString(), this);
        }

        public void SetParameter(string parameterValue) {
            SetParameter(parameterValue, this);
        }

        public void SetParameter(string parameterValue, Menu menu) {
            menu.openParameter = parameterValue + " IN";
            menu.closedParameter = parameterValue + " OUT";
        }

        public void SetAnimationSpeed(float speedValue) {
            _animator.speed = speedValue;
        }

#if UNITY_EDITOR
        protected void OnValidate() {
            _animator = GetComponent<Animator>();
        }
#endif
    }
}