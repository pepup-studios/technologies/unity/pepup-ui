﻿using System.Collections.Generic;
using UnityEngine;
using PepUp.Events;
using PepUp.Utility;

namespace PepUp.UI {
    [AddComponentMenu("PepUp/UI/UI Manager")]
    public class UIManager : MonoBehaviour {

        public bool doFirstTimeLoad = false;
        public string saveFile = "menuMapInfo";
        public EventObject firstMenuNotLoaded;
        public MenuMap[] menus;
        public RegisterEvent DoOpenMenu;

        protected Menu currentMenu = null;
        protected Dictionary<string, Menu> menuMap;

        protected void Start() {
            if (doFirstTimeLoad) doFirstTimeLoad = !Load();

            menuMap = new Dictionary<string, Menu>();
            int len = menus.Length;
            Menu firstMenu = null;
            for (int i = 0; i < len; i++) {
                MenuMap menu = menus[i];
                Menu menuObj = menu.menuObject;
                menuObj.InitParameter();
                menuMap.Add(menuObj.MenuName, menuObj);
                if (menu.isStart) {
                    currentMenu = menuObj;
                }
                if (doFirstTimeLoad && menu.isFirst) {
                    firstMenu = menuObj;
                    menu.isFirst = false;
                }
            }
            if (currentMenu == null) return;

            if (doFirstTimeLoad) {
                if (firstMenu != null) currentMenu = firstMenu;

                Save();
            } else {
                firstMenuNotLoaded?.Invoke(this);
            }

            currentMenu.Open(null);
            //if (currentMenu.header != "") {
            //    TransitionHeaderWithTypeOnOpen();
            //}
        }

        protected bool Load() {
            return SaveUtility.LoadFromBinaryFormatter(saveFile, out bool value);
        }

        protected void Save() {
            SaveUtility.SaveAsBinaryFormatter(true, saveFile);
        }

        protected void OnEnable() {
            DoOpenMenu.Register(this, "OpenMenu");
        }

        protected void OnDisable() {
            DoOpenMenu.Unregister();
        }

        public void OpenMenu(params object[] objs) {
            OpenMenu((string)objs[0]);
        }

        public void OpenMenu(string name) {
            currentMenu = currentMenu.OpenNew(menuMap[name]);
            //if (currentMenu.header != "") {
            //    TransitionHeaderWithTypeOnOpen();
            //}
        }

        public void CloseMenu()
        {
            currentMenu = currentMenu.SelfClose();
            //if (currentMenu.header != "") {
            //    TransitionHeaderWithTypeOnOpen();
            //}
        }

        public void OpenMenuWithDefaultTransition(string name) {
            currentMenu.SetParameter("Default", menuMap[name]);
            OpenMenu(name);
        }

        public void OpenMenuWithPopupTransition(string name) {
            currentMenu.SetParameter("Popup", menuMap[name]);
            OpenMenu(name);
        }

        public void OpenMenuWithFadeTransition(string name) {
            currentMenu.SetParameter("Fade", menuMap[name]);
            OpenMenu(name);
        }

        public void OpenMenuWithFlowTransition(string name) {
            currentMenu.SetParameter("Flow", menuMap[name]);
            OpenMenu(name);
        }

        public void OpenMenuWithDisappearTransition(string name) {
            currentMenu.SetParameter("Disappear", menuMap[name]);
            OpenMenu(name);
        }

        //private void TransitionHeaderWithTypeOnOpen() {
        //    string headerName = currentMenu.header;
        //    switch (menuMap[headerName].defaultTransition) {
        //        case TransitionTypes.Default:
        //            OpenMenuWithDefaultTransition(headerName);
        //            break;
        //        case TransitionTypes.Popup:
        //            OpenMenuWithPopupTransition(headerName);
        //            break;
        //        case TransitionTypes.Fade:
        //            OpenMenuWithFadeTransition(headerName);
        //            break;
        //        case TransitionTypes.Flow:
        //            OpenMenuWithFlowTransition(headerName);
        //            break;
        //    }
        //}

        public void SetAnimationSpeed(float speedValue) {
            currentMenu.SetAnimationSpeed(speedValue);
        }
    }

    [System.Serializable]
    public class MenuMap {
        public Menu menuObject;
        public bool isStart;
        public bool isFirst;
    }

    public enum MenuTypes { Page, Popup };
    public enum TransitionTypes { Default, Popup, Fade, Flow, Disappear };
}