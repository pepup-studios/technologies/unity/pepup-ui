﻿using UnityEngine;
using UnityEngine.UI;
using PepUp.Events;

namespace PepUp.UI {
    [RequireComponent(typeof(ScrollRect))]
    public class HorizontalScrollRectController : MonoBehaviour {

        public HorizontalScrollSnap horizontalScrollSnap;
        public float speed = 5;
        public bool blockScrollRectOnUpdate = true;
        public bool onlyUpdateTargetContent = true;
        public RegisterEvent DoUpdate;

        [SerializeField, HideInInspector]
        protected ScrollRect scrollRect;

        protected float currPos;
        protected float target;
        protected float time;

        protected void Awake() {
            DoUpdate.Register(this, "UpdateScrollRect");
            Deactive();
        }

        protected void OnDestroy() {
            DoUpdate.Unregister();
        }

        protected void UpdateScrollRect(params object[] objs) {
            UpdateScrollRect((int)objs[0]);
        }

        public void UpdateScrollRect(int index) {
            currPos = scrollRect.horizontalNormalizedPosition;
            int total = horizontalScrollSnap.maxItemCount - 1;
            if (total > 0) {
                target = index / (total * 1f);
            } else {
                target = 0;
            }
            time = 0;
            if (blockScrollRectOnUpdate) scrollRect.enabled = false;
            if (onlyUpdateTargetContent) {
                horizontalScrollSnap.enabled = false;
            } else {
                horizontalScrollSnap.StartDrag();
            }
            Active();
        }

        protected void Update() {
            scrollRect.horizontalNormalizedPosition = Mathf.Lerp(currPos, target, time);
            time += Time.deltaTime * speed;
            if (time > 1f) {
                scrollRect.horizontalNormalizedPosition = target;
                if (blockScrollRectOnUpdate) scrollRect.enabled = true;
                if (onlyUpdateTargetContent) {
                    horizontalScrollSnap.UpdateScrollRect();
                    horizontalScrollSnap.enabled = true;
                } else {
                    horizontalScrollSnap.EndDrag();
                }
                Deactive();
            }
        }

        public void Active() {
            enabled = true;
        }

        public void Deactive() {
            enabled = false;
        }

#if UNITY_EDITOR
        protected void OnValidate() {
            if (Application.isPlaying) return;
            scrollRect = GetComponent<ScrollRect>();
        }
#endif
    }
}