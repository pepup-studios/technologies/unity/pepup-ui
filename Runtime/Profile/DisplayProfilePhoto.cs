﻿using PepUp;
using UnityEngine;
using UnityEngine.UI;
using PepUp.Events;

public class DisplayProfilePhoto : MonoBehaviour {

    public RawImage profilePhoto;
    public TextureOption profilePhotoData;

    [Space]
    public RegisterEvent DoUpdate;

    protected void OnEnable() {
        UpdatePhoto();

        DoUpdate.Register(this, "UpdatePhoto");
    }

    protected void OnDisable() {
        DoUpdate.Unregister();
    }

    protected void UpdatePhoto(params object[] objs) {
        UpdatePhoto();
    }

    public void UpdatePhoto() {
        profilePhoto.texture = profilePhotoData.value;
    }
}