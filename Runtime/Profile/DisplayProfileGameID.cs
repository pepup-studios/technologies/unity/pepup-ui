﻿using PepUp;
using PepUp.Events;
using TMPro;
using UnityEngine;

public class DisplayProfileGameID : MonoBehaviour {

    public TextMeshProUGUI gameIDText;
    public TextMeshPro gameIDTextMesh;
    public StringOption gameIDData;

    [Space]
    public RegisterEvent DoUpdate;

    protected void OnEnable() {
        UpdateText();

        DoUpdate.Register(this, "UpdateText");
    }

    protected void OnDisable() {
        DoUpdate.Unregister();
    }

    protected void UpdateText(params object[] objs) {
        UpdateText();
    }

    public void UpdateText() {
        if (gameIDText != null) gameIDText.text = gameIDData.value;
        if (gameIDTextMesh != null) gameIDTextMesh.text = gameIDData.value;
    }
}