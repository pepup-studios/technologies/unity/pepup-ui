﻿using PepUp.Events;
using PepUp.UI;
using UnityEngine;

public class ProfileSaveButton : MonoBehaviour
{
    public UIManager uiManager;
    public string transitionMenuName;

    [Space]
    public RegisterEvent DoActive;
    public RegisterEvent DoDeactive;
    public RegisterEvent DoSave;

    protected void OnEnable()
    {
        DoActive.Register(this, "Active");
        DoDeactive.Register(this, "Deactive");
    }

    protected void OnDisable()
    {
        DoActive.Unregister();
        DoDeactive.Unregister();
    }

    protected void Active(params object[] objs) {
        DoSave.Register(this, "Save");
    }

    protected void Deactive(params object[] objs)
    {
        DoSave.Unregister();
    }

    protected void Save(params object[] objs) {
        uiManager.OpenMenu(transitionMenuName);
    }
}
