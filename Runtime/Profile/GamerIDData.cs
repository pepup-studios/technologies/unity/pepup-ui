﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/Versus/Game Tag Data")]
public class GamerIDData : ScriptableObject {

    public string[] gameIds;

    protected List<string> gameIdList;

    public string GetRandomID() {
        return gameIds[Random.Range(0, gameIds.Length)];
    }

    public string GetUniqueRandomID() {
        if (gameIdList == null || gameIdList.Count == 0) {
            gameIdList = new List<string>(gameIds);
        }
        int index = Random.Range(0, gameIdList.Count);
        string gameId = gameIdList[index];
        gameIdList.RemoveAt(index);
        return gameId;
    }

#if UNITY_EDITOR

    public TextAsset gameTexts;

    protected void OnValidate() {
        if (gameTexts != null) {
            gameIds = gameTexts.text.Split('\n');
        }
    }

#endif
}
