﻿using PepUp;
using PepUp.Events;
using TMPro;
using UnityEngine;

public class ReadProfileGameID : MonoBehaviour {

    public TMP_InputField gameIDInput;
    public StringOption gameIDData;

    [Space]
    public RegisterEvent DoUpdate;

    protected void OnEnable() {
        UpdateText();

        DoUpdate.Register(this, "UpdateText");
    }

    protected void OnDisable() {
        DoUpdate.Unregister();
    }

    protected void UpdateText(params object[] objs) {
        UpdateText();
    }

    public void UpdateText() {
        gameIDData.value = gameIDInput.text;
    }
}