﻿using PepUp.Events;
using UnityEngine;
using PepUp;

public class RandomGamerIDController : MonoBehaviour {

    public GamerIDData gameIDData;
    public StringOption gameID;

    public EventObject OnUpdateID;
    public event EventAction OnUpdateIDEvent;

    [Space]
    public RegisterEvent DoUpdateID;

    protected void Awake() {
        UpdateID();
    }

    protected void OnEnable() {
        DoUpdateID.Register(this, "UpdateID");
    }

    protected void OnDisable() {
        DoUpdateID.Unregister();
    }

    protected void UpdateID(params object[] objs) {
        UpdateID();
    }

    public void UpdateID() {
        string newID = gameIDData.GetRandomID();
        gameID.value = newID;

        OnUpdateID?.Invoke(this, gameID);
        OnUpdateIDEvent?.Invoke(this, gameID);
    }
}
