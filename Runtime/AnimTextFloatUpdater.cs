﻿using UnityEngine;

namespace PepUp.UI {
    public class AnimTextFloatUpdater : TextFloatUpdater {

        public float lerpSpeed = 1f;

        protected float currentValue;
        protected float t = 0f;

        protected float previousValue;

        protected override void Awake() {
            enabled = false;
            base.Awake();
        }

        public override void UpdateText(float value) {
            currentValue = value;
            if (currentValue - previousValue == 0) {
                base.UpdateText(value);
                previousValue = currentValue;
                return;
            }
            t = 0;
            enabled = true;
        }

        protected virtual void Update() {
            LerpText();
        }

        protected virtual void LerpText() {
            t = t + lerpSpeed * Time.deltaTime;
            float lerpValue = Mathf.Lerp(previousValue, currentValue, t);
            if ((currentValue - lerpValue) == 0 || lerpValue <= 0) {
                enabled = false;
                t = 0f;
                previousValue = currentValue;
            }
            float score = lerpValue;
            base.UpdateText(score);
        }

        public override void Reset() {
            if (textBox == null) return;
            textBox.text = resetText;
            previousValue = 0;
        }
    }
}