﻿using UnityEngine;
using PepUp.Events;

namespace PepUp.UI {
    [AddComponentMenu("PepUp/UI/Element"), RequireComponent(typeof(Animator))]
    public class UIElement : MonoBehaviour {

        [Range(0, 5)]
        public float activePlaybackSpeed = 1;
        [Range(0, 5)]
        public float deactivePlaybackSpeed = 1;

        [Space]
        public RegisterEvent OnActive;
        public RegisterEvent OnDeactive;

        public event EventAction OnActiveComplete;
        public event EventAction OnDeactiveComplete;

        [SerializeField, HideInInspector]
        protected Animator animator;

        protected void OnEnable() {
            OnActive.Register(this, "Active");
            OnDeactive.Register(this, "Deactive");
        }

        protected void OnDisable() {
            OnActive.Unregister();
            OnDeactive.Unregister();
        }

        protected void Active(params object[] objs) {
            Active();
        }

        public void Active() {
            animator.speed = activePlaybackSpeed;
            animator.SetBool("Open", true);
        }

        //Called by animator anim
        public void DoActiveComplete() {
            OnActiveComplete?.Invoke();
        }

        protected void Deactive(params object[] objs) {
            Deactive();
        }

        public void Deactive() {
            animator.speed = deactivePlaybackSpeed;
            animator.SetBool("Open", false);
        }

        //Called by animator anim
        public void DoDeactiveComplete() {
            OnDeactiveComplete?.Invoke();
        }

#if UNITY_EDITOR
        protected void OnValidate() {
            if (Application.isPlaying) return;
            animator = GetComponent<Animator>();
        }
#endif
    }
}