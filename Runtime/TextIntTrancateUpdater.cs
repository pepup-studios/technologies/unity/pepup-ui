﻿using UnityEngine;

namespace PepUp.UI {
    public class TextIntTrancateUpdater : TextIntUpdater {

        public override void UpdateText(int value) {
            int zeroCount = (int)Mathf.Log10(value);
            string stringValue = "0";
            switch (zeroCount) {
                case 3:
                    stringValue = (value / 1000).ToString("F") + "K";
                    break;
                case 4:
                    stringValue = (value / 10000).ToString("F") + "M";
                    break;
                case 5:
                    stringValue = (value / 10000).ToString("F") + "B";
                    break;

            }
            textBox.text = prefix + stringValue + suffix;
        }
    }
}
