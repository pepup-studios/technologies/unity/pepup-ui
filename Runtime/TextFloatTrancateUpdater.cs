﻿using UnityEngine;

namespace PepUp.UI {
    public class TextFloatTrancateUpdater : TextFloatUpdater {

        public override void UpdateText(float value) {
            int zeroCount = (int)Mathf.Log10(value);
            string stringValue;
            float format = decimalPoint > 0 ? 10 * decimalPoint : 1;
            if (zeroCount >= 3 && zeroCount < 6)
            {
                stringValue = (System.Math.Truncate((value / 1000) * format) / format).ToString() + "K";
            }
            else if (zeroCount >= 6 && zeroCount < 9)
            {
                stringValue = (System.Math.Truncate((value / 1000000) * format) / format).ToString() + "M";
            }
            else if (zeroCount >= 9 && zeroCount < 12)
            {
                stringValue = (System.Math.Truncate((value / 1000000000) * format) / format).ToString() + "B";
            }
            else
            {
                stringValue = ((int)value).ToString();
            }
            textBox.text = prefix + stringValue + suffix;
        }
    }
}