﻿using PepUp.Events;
using PepUp.Utility;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;

public class GraphicQualityController : MonoBehaviour {

    //public TextMeshProUGUI textMesh;
    [SerializeField] protected LocaleStringUpdater localeStringUpdater;
    [SerializeField] protected string msgFormat = "{0}";
    public int defaultIndex = 1;
    public string[] localeKeys = { "VERY_LOW", "LOW", "MEDIUM", "HIGH", "VERY_HIGH" };
    //public string[] values;
    public CanvasGroup leftButton;
    public CanvasGroup rightButton;
    public bool loadOnAwake;
    public RenderPipelineAsset[] renderPipelineAssets;
    public RegisterEvent DoSave;

    protected int currIndex;

    private void Awake() {
        DoSave.Register(this, "Save");

        if (loadOnAwake) {
            LoadSensitivity();
        }
        UpdateQuality();
    }

    private void OnDestroy() {
        DoSave.Unregister();
    }

    public void Save(params object[] objs) {
        SaveSensitivity();
    }

    public void LoadSensitivity() {
        if (!SaveUtility.LoadFromBinaryFormatter("graphicInfo", out currIndex)) {

            float ram = SystemInfo.systemMemorySize;

            if (ram <= 2000)
                currIndex = 2;
            else if (ram <= 4000)
                currIndex = 3;
            else if (ram > 4000)
                currIndex = 4;
            else
                currIndex = defaultIndex;
        }
        if (currIndex == 0)
        {
            SetButton(leftButton, false);
        }
        else if (currIndex == localeKeys.Length - 1)
        {
            SetButton(rightButton, false);
        }
    }

    public void SaveSensitivity() {
        SaveUtility.SaveAsBinaryFormatter(currIndex, "graphicInfo");
    }

    public void Previous() {
        currIndex--;
        if (currIndex <= 0) {
            currIndex = 0;
            SetButton(leftButton, false);
        }
        SetButton(rightButton, true);

        UpdateQuality();
    }

    public void Next() {
        currIndex++;
        int len = localeKeys.Length - 1;
        if (currIndex >= len) {
            currIndex = len;
            SetButton(rightButton, false);
        }
        SetButton(leftButton, true);

        UpdateQuality();
    }

    public void UpdateQuality() {
        QualitySettings.renderPipeline = GraphicsSettings.renderPipelineAsset = renderPipelineAssets[currIndex];

        LocText locText = new LocText(msgFormat, new object[] { new LocKey(localeKeys[currIndex]) });
        localeStringUpdater.UpdateText(locText);
    }

    protected void SetButton(CanvasGroup btn, bool active) {
        btn.alpha = active ? 1 : 0;
        btn.interactable = btn.blocksRaycasts = active;
    }
}
