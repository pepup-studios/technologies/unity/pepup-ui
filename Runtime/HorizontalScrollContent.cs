﻿using UnityEngine;
using UnityEngine.UI;
using PepUp.Utility;

namespace PepUp.UI {
    //[RequireComponent(typeof(Image)), RequireComponent(typeof(Animator))]
    public class HorizontalScrollContent : MonoBehaviour {

        [HideInInspector]
        public RectTransform rectTransform;
        //[HideInInspector]
        //public Image image;
        //[HideInInspector]
        //public Animator animator;

        public int index { get; private set; }

        public virtual void Active() {
            //animator.SetBool("Active", true);
        }

        public virtual void Deactive() {
            //animator.SetBool("Active", false);
        }

        public virtual void UpdateContent(int index, float distance) {
            this.index = index;
        }

        public virtual void SaveItem(string fileName) {
            SaveUtility.SaveAsBinaryFormatter(index, fileName);
        }

#if UNITY_EDITOR
        protected virtual void OnValidate() {
            if (Application.isPlaying) return;
            rectTransform = transform as RectTransform;
            //image = GetComponent<Image>();
            //animator = GetComponent<Animator>();
        }
#endif
    }
}