﻿using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class AnimateEllipsisTextUpdater : MonoBehaviour {

    //public float animWaitTime;
    public string prefix;
    public string suffix;

    //public RegisterEvent DoStartAnim;
    //public RegisterEvent DoStopAnim;

    [SerializeField, HideInInspector]
    protected TextMeshProUGUI textMesh;

    public void DotZero() {
        textMesh.text = prefix.Replace("\\n", "\n") + suffix;
    }

    public void DotOne() {
        textMesh.text = prefix.Replace("\\n", "\n") + "." + suffix;
    }

    public void DotTwo() {
        textMesh.text = prefix.Replace("\\n", "\n") + ".." + suffix;
    }

    public void DotThree() {
        textMesh.text = prefix.Replace("\\n", "\n") + "..." + suffix;
    }

    //protected int counter;

    //protected void OnEnable() {
    //    DoStartAnim.Register(this, "StartAnimation");
    //    DoStopAnim.Register(this, "StopAnimation");
    //}

    //protected void OnDisable() {
    //    DoStartAnim.Unregister();
    //    DoStopAnim.Unregister();
    //}

    //protected IEnumerator Update() {
    //    while (true) {
    //        textMesh.text = prefix + suffix;
    //        yield
    //    }
    //}

    //protected void StartAnimation(params object[] objs) {
    //    StartAnimation();
    //}

    //protected void StopAnimation(params object[] objs) {
    //    StopAnimation();
    //}

    //public void StartAnimation() {
    //    enabled = true;
    //}

    //public void StopAnimation() {
    //    enabled = false;
    //}

#if UNITY_EDITOR
    protected void OnValidate() {
        if (Application.isPlaying) return;
        textMesh = GetComponent<TextMeshProUGUI>();
    }
#endif
}
