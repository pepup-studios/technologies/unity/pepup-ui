﻿using PepUp.Events;
using UnityEngine;
using UnityEngine.UI;

namespace PepUp.UI {
    [RequireComponent(typeof(Toggle)), AddComponentMenu("PepUp/UI/Toggle Canvas Group")]
    public class ToggleCanvasGroup : MonoBehaviour
    {
        public CanvasGroup offCanvas;
        public EventObject OnToggleUpdate;
        public EventObject OnToggleUpdateFalse;
        public EventObject OnToggleUpdateTrue;

        public event EventAction OnToggleUpdateEvent;
        public event EventAction OnToggleUpdateFalseEvent;
        public event EventAction OnToggleUpdateTrueEvent;

        [SerializeField, HideInInspector]
        protected Toggle toggle;

        public void Toggle() {
            offCanvas.alpha = (!toggle.isOn) ? 1 : 0;
            offCanvas.interactable = offCanvas.blocksRaycasts = !toggle.isOn;

            if (toggle.isOn)
            {
                OnToggleUpdateTrue?.Invoke(this, toggle);
                OnToggleUpdateTrueEvent?.Invoke(this, toggle);
            }
            else {
                OnToggleUpdateFalse?.Invoke(this, toggle);
                OnToggleUpdateFalseEvent?.Invoke(this, toggle);
            }

            OnToggleUpdate?.Invoke(this, toggle);
            OnToggleUpdateEvent?.Invoke(this, toggle);
        }

#if UNITY_EDITOR
        protected void OnValidate() {
            toggle = GetComponent<Toggle>();
        }
#endif
    }
}