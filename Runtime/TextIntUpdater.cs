﻿using PepUp.Events;
using TMPro;
using UnityEngine;

namespace PepUp.UI
{
    public class TextIntUpdater : MonoBehaviour
    {
        public TextMeshProUGUI textBox;
        public string prefix;
        public string suffix;

        public float updateText {
            get {
                return 0;
            }
            set {
                UpdateText((int)value);
            }
        }

        public RegisterEvent DoUpdateText;
        public RegisterEvent DoReset;

        protected string resetText = "0";

        protected virtual void Awake()
        {
            DoUpdateText.Register(this, "UpdateText");
            Reset();
        }

        private void OnDestroy()
        {
            DoUpdateText.Unregister();
        }

        protected virtual void UpdateText(params object[] objs)
        {
            try {
                UpdateText((int) objs[1]);
            }
            catch {
                UpdateText((objs[0] as IntObject).value);
            }
        }

        public virtual void UpdateText(int value)
        {
            textBox.text = prefix + value + suffix;
        }

        protected void Reset(params object[] objs)
        {
            Reset();
        }

        public virtual void Reset()
        {
            if (textBox == null) return;
            textBox.text = prefix + resetText + suffix;
        }
    }
}
