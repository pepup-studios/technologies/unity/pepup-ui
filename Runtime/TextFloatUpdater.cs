﻿using PepUp.Events;
using TMPro;
using UnityEngine;

namespace PepUp.UI
{
    public class TextFloatUpdater : MonoBehaviour
    {
        public TextMeshProUGUI textBox;
        public string prefix;
        public string suffix;
        public float decimalPoint;

        public RegisterEvent DoUpdateText;
        public RegisterEvent DoReset;

        protected string resetText = "0";

        protected virtual void Awake()
        {
            Reset();

            DoUpdateText.Register(this, "UpdateText");
            DoReset.Register(this, "Reset");
        }

        protected virtual void OnDestroy()
        {
            DoUpdateText.Unregister();
            DoReset.Unregister();
        }

        protected virtual void UpdateText(params object[] objs)
        {
            try {
                UpdateText((float) objs[1]);
            }
            catch {
                UpdateText((objs[0] as FloatObject).value);
            }
        }

        public virtual void UpdateText(float value)
        {
            textBox.text = prefix + value.ToString("F" + decimalPoint) + suffix;
        }

        protected void Reset(params object[] objs)
        {
            Reset();
        }

        public virtual void Reset()
        {
            if (textBox == null) return;
            textBox.text = resetText;
        }
    }
}
