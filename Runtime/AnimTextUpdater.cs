﻿using UnityEngine;

namespace PepUp.UI
{
    public class AnimTextUpdater : TextIntUpdater
    {
        public float lerpSpeed = 1f;

        protected int currentValue;
        protected bool lerpText;
        protected float t = 0f;

        protected int previousValue;

        protected override void Awake() {
            base.Awake();
            enabled = false;
        }

        public override void UpdateText(int numberOfCoins)
        {
            t = 0f;
            previousValue = 0;
            currentValue = numberOfCoins;
            //if ((currentValue - previousValue) == 1) {
            //    base.UpdateText(numberOfCoins);
            //    previousValue = currentValue;
            //    return;
            //}

            //Temp
            int score = (int) currentValue;
            textBox.text = score.ToString();

            //enabled = true;
        }

        private void Update()
        {
            LerpText();
        }

        private void LerpText()
        {
            float lerpValue = Mathf.Lerp(previousValue, currentValue, t);
            t += lerpSpeed * Time.deltaTime;
            if (Mathf.Abs(currentValue - lerpValue) < 0.1f)
            {
                enabled = false;
                t = 0f;
                lerpValue = previousValue = currentValue;
            }
            int score = (int)lerpValue;
            textBox.text = score.ToString();
        }

        public override void Reset() {
            //if (textBox == null) return;
            //base.Reset();
            //previousValue = 0;
        }
    }
}