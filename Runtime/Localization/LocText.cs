﻿public class LocText
{
    public object[] args;
    public string formatter;
    public string value { get; protected set; }

    protected LocaleManager localeManager;

    public LocText()
    {
    }

    public LocText(string formatter, object[] args) {
        localeManager = LocaleManager.instance;
        this.args = args;
        this.formatter = formatter;
        UpdateValue();
    }

    public void UpdateValue() {
        value = GetLocalizedValue();
    }

    protected string GetLocalizedValue() {
        object[] localizeArgs = new object[args.Length];

        for (int i = 0; i < args.Length; i++)
        {
            if(args[i] is LocKey locKey){
                localizeArgs[i] = string.IsNullOrEmpty(locKey.key)? string.Empty : localeManager.GetLocalizeString(locKey.key);
            }else {
                localizeArgs[i] = args[i];
            }
        }
        return string.Format(formatter, localizeArgs);
    }

    //protected string GetLocalizeValueByKey(string localeKey) {
    //    if (string.IsNullOrEmpty(localeKey)) return string.Empty;
    //    return localeManager.GetLocalizeString(localeKey);
    //}
}

[System.Serializable]
public class LocKey {
    public string key;

    public LocKey() { }

    public LocKey(string key) {
        this.key = key;
    }
}
