﻿using PepUp.Collections;
using PepUp.Events;
using PepUp.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;

public class LocaleManager : Singleton<LocaleManager>
{
    [SerializeField] protected string fileName = "Localize Info";
    public LocalizationSettings localeSettings;
    [SerializeField] protected EventObject OnLocaleChanged;

    public int selectedIndex { get; protected set; } = -1;

    protected LocalizedStringDatabase localizedStringDatabase;

    protected override void Awake()
    {
        InitSingleton(this);
        StartCoroutine(GetInitOperation());
    }

    IEnumerator GetInitOperation()
    {
        yield return localeSettings.GetInitializationOperation();
        LoadSelectedLocale();
        localizedStringDatabase = localeSettings.GetStringDatabase();
    }

    protected void LoadSelectedLocale()
    {
        Locale locale = new Locale();
        if (SaveUtility.LoadFromJson(fileName, locale)) {
            Debug.Log($"LoadFromJson name : {locale.LocaleName}");
            localeSettings.SetSelectedLocale(locale);
        }
    }

    protected void SaveSelectedLocale() {
        SaveUtility.SaveAsJson(localeSettings.GetSelectedLocale(), fileName);
    }

    public List<Locale> GetAvailableLocales() {
        return localeSettings.GetAvailableLocales().Locales;
    }

    public Locale GetSelectedLocale()
    {
        return localeSettings.GetSelectedLocale();
    }

    public void SetLocale(int index)
    {
        selectedIndex = index;
        localeSettings.SetSelectedLocale(GetAvailableLocales()[index]);
        SaveSelectedLocale();
        OnLocaleChanged?.Invoke(this);
    }

    public string GetLocalizeString(string key) {
        string value = "";
        //try
        //{
            value = localizedStringDatabase.GetLocalizedString(key);
        //}
        //catch { Debug.LogError($"Can't Get Localize value for {key}");}
        return value;
    }
}