﻿using PepUp.Events;
using UnityEngine;

public class CustomLocaleStringSetter : LocaleStringUpdater
{
    [SerializeField] protected LocKey[] localeKeys;
    [SerializeField] protected string msgFormat = "{0} {1}";
    [SerializeField] protected RegisterEvent DoSetLocaleString;

    protected override void OnEnable()
    {
        base.OnEnable();
        DoSetLocaleString.Register(this, nameof(SetLocaleString));
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        DoSetLocaleString.Unregister();
    }

    protected void Start() {
        SetLocaleString();
    }

    protected void SetLocaleString(params object[] objs)
    {
        SetLocaleString();
    }

    protected void SetLocaleString()
    {
        LocText locText = new LocText(msgFormat, localeKeys);
        UpdateText(locText);
    }
}
