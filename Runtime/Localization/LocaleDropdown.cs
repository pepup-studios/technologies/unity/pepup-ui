﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Localization;
using static TMPro.TMP_Dropdown;
using PepUp.Events;

public class LocaleDropdown : MonoBehaviour
{
    [SerializeField] protected TMP_Dropdown  dropdown;

    public RegisterEvent DoUpdateSelection;

    protected LocaleManager localeManager;
    protected List<Locale> availableLocales;
    protected Locale selectedLocale;

    protected void Start()
    {
        OptionDataList optionsData = new OptionDataList();
        availableLocales = localeManager.GetAvailableLocales();
        selectedLocale = localeManager.GetSelectedLocale();

        int selected = 0;
        for (int i = 0; i < availableLocales.Count; ++i)
        {
            Locale locale = availableLocales[i];

            //Debug.Log($"Test Locale i = {i} Locale : {locale.name} | Selected Locale : {selectedLocale.LocaleName}");
            if (locale.LocaleName.Equals(selectedLocale.LocaleName)) {
                selected = i;
                //Debug.Log($"Test Locale Selected Locale {selectedLocale.LocaleName}");
            }
            optionsData.options.Add(new OptionData(locale.name));
        }
        dropdown.options = optionsData.options;
        
        dropdown.value = selected;
        dropdown.onValueChanged.AddListener(LocaleSelected);
    }

    protected void OnEnable()
    {
        localeManager = LocaleManager.instance;
        DoUpdateSelection.Register(this, nameof(UpdateSelection));
    }

    protected void OnDisable()
    {
        DoUpdateSelection.Unregister();
    }

    protected void UpdateSelection(params object[] objs) {
        if (localeManager.selectedIndex != -1)
        {
            dropdown.value = localeManager.selectedIndex;
        }
    }

    protected void LocaleSelected(int index)
    {
        localeManager.SetLocale(index);
    }
}
