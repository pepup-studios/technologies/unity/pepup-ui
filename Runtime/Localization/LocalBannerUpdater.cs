﻿using PepUp;
using PepUp.Events;
using TMPro;
using UnityEngine;

public class LocalBannerUpdater : MonoBehaviour
{
    [SerializeField] protected TextMeshProUGUI textBox;
    [SerializeField] protected string msgFormat = "{0}";
    [SerializeField] protected RegisterEvent DoUpdateText;

    protected LocKey localeKey;

    protected virtual void Awake()
    {
        DoUpdateText.Register(this, nameof(UpdateText));
    }

    protected virtual void OnDestroy()
    {
        DoUpdateText.Unregister();
    }

    protected void UpdateText(params object[] objs)
    {
        string locKey = (objs[0] as StringObject).value;
        LocText locText = new LocText(msgFormat, new object[] { new LocKey(locKey)});
        textBox.text = locText.value; 
    }
}
