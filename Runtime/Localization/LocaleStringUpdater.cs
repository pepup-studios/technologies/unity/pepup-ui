﻿using PepUp;
using PepUp.Events;
using PepUp.UI;
using UnityEngine;

public class LocaleStringUpdater : TextStringUpdater
{
    [Space]
    [SerializeField] protected RegisterEvent DoUpdateTextLanguage;

    protected LocText locText;

    protected override void OnEnable()
    {
        base.OnEnable();
        DoUpdateTextLanguage.Register(this, nameof(UpdateTextLanguage));
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        DoUpdateTextLanguage.Unregister();
    }

    protected void UpdateTextLanguage(params object[] objs)
    {
        if (locText == null) return;
        locText.UpdateValue();
        UpdateText(locText);
    }

    protected override void UpdateText(params object[] objs)
    {
        Debug.Log($"LocaleStringUpdater UpdateText...");
        LocText tempLocText;
        try
        {
            tempLocText = objs[1] as LocText;
            if (tempLocText == null) tempLocText = objs[0] as LocText;
            UpdateText(tempLocText);
        }
        catch { }
    }

    public void UpdateText(LocText newlocText)
    {
        locText = newlocText;
        if (locText == null) {
            Debug.LogError("locText is Null");
            return;
        }
        UpdateText(locText.value);
    }
}
