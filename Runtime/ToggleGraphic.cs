﻿using UnityEngine;
using UnityEngine.UI;

namespace PepUp.UI {
    [RequireComponent(typeof(Toggle)), AddComponentMenu("PepUp/UI/Toggle Graphic")]
    public class ToggleGraphic : MonoBehaviour {

        public Graphic offGraphic;

        [SerializeField, HideInInspector]
        protected Toggle toggle;

        public void ToggleOff() {
            offGraphic.enabled = !toggle.isOn;
        }

#if UNITY_EDITOR
        protected void OnValidate() {
            toggle = GetComponent<Toggle>();
        }
#endif
    }
}