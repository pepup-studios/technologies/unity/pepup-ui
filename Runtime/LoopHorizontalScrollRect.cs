﻿using PepUp.Events;
using UnityEngine;
using UnityEngine.UI;

namespace PepUp.UI {
    public class LoopHorizontalScrollRect : MonoBehaviour {

        public ScrollRect scrollRect;
        public RectTransform centerTransform;
        public float maxDistance;
        public LoopScrollRectContent[] contents;
        public float colWidth;

        [Space]
        public RegisterEvent DoActive;
        public RegisterEvent DoDeactive;

        protected float colWidthSum;

        protected void Awake() {
            colWidthSum = CalculateColWidthSum();

            DoActive.Register(this, "Active");
            DoDeactive.Register(this, "Deactive");

            Deactive();
        }

        protected void OnDestroy() {
            DoActive.Unregister();
            DoDeactive.Unregister();
        }

        protected void Active() {
            enabled = true;
        }

        protected void Deactive() {
            enabled = false;
        }

        protected void Update() {
            UpdateScrollRect();
        }

        public void UpdateScrollRect() {
            colWidthSum = CalculateColWidthSum();
            int len = contents.Length;
            for (int i = 0; i < len; i++) {
                Vector3 pos = contents[i].rectTransform.position;
                if (pos.x >= maxDistance) {
                    //Move right
                }
                else if (pos.x <= -maxDistance) {
                    //Move left
                }
            }
        }

        protected float CalculateColWidthSum() {
            return contents.Length * colWidth;
        }
    }
}