﻿using UnityEngine;

namespace PepUp.UI {
    public class TextAnimFloatTrancateUpdater : AnimTextFloatUpdater {

        public FloatObject floatObject;

        protected override void Awake() {
            base.Awake();

            if (floatObject != null) {
                float value = floatObject.value;
                TrancateText(value);
                previousValue = value;
            }
        }

        public override void UpdateText(float value) {
            currentValue = value;
            if (currentValue - previousValue == 0) {
                TrancateText(value);
                previousValue = currentValue;
                return;
            }
            t = 0;
            enabled = true;
        }

        protected override void LerpText() {
            t = t + lerpSpeed * Time.deltaTime;
            float lerpValue = Mathf.Lerp(previousValue, currentValue, t);
            if ((currentValue - lerpValue) == 0 || lerpValue <= 0) {
                enabled = false;
                t = 0f;
                previousValue = currentValue;
            }
            float score = lerpValue;
            TrancateText(score);
        }

        protected void TrancateText(float value) {
            int zeroCount = (int) Mathf.Log10(value);
            string stringValue;
            if (zeroCount >= 3 && zeroCount < 6) {
                stringValue = (value / 1000).ToString("F" + decimalPoint) + "K";
            } else if (zeroCount >= 6 && zeroCount < 9) {
                stringValue = (value / 1000000).ToString("F" + decimalPoint) + "M";
            } else if (zeroCount >= 9 && zeroCount < 12) {
                stringValue = (value / 1000000000).ToString("F" + decimalPoint) + "B";
            } else {
                stringValue = value.ToString("F0");
            }
            textBox.text = prefix + stringValue + suffix;
        }
    }
}