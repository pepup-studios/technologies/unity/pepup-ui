﻿using UnityEngine;
using PepUp.Events;

[RequireComponent(typeof(Animator))]
public class SimpleAnimatorController : MonoBehaviour {

    public EventObject OnStart;
    public EventObject OnComplete;

    [Space]
    public RegisterEvent DoBoolTrue;
    public RegisterEvent DoBoolFalse;

    public event EventAction OnCompleteEvent;

    [SerializeField, HideInInspector]
    protected Animator animator;

    protected virtual void OnEnable() {
        DoBoolTrue.Register(this, "BoolTrue");
        DoBoolFalse.Register(this, "BoolFalse");
    }

    protected virtual void OnDisable() {
        DoBoolTrue.Unregister();
        DoBoolFalse.Unregister();
    }

    public virtual void BoolTrue(params object[] objs) {
        string parameterKey = (string) objs[0];
        animator.SetBool(parameterKey, true);
    }

    public virtual void BoolTrue(string parameterKey) {
        animator.SetBool(parameterKey, true);
    }

    public virtual void BoolFalse(params object[] objs) {
        string parameterKey = (string) objs[0];
        animator.SetBool(parameterKey, false);
    }

    public virtual void BoolFalse(string parameterKey) {
        animator.SetBool(parameterKey, false);
    }

    public void DoStart() {
        OnStart?.Invoke(this);
    }

    public void DoCompleted() {
        OnCompleteEvent?.Invoke();
        OnComplete?.Invoke();
    }

#if UNITY_EDITOR
    protected void OnValidate() {
        if (Application.isPlaying) return;
        animator = GetComponent<Animator>();
    }
#endif
}
