﻿using PepUp.Events;
using TMPro;
using UnityEngine;

namespace PepUp.UI
{
    public class TextStringUpdater : MonoBehaviour
    {
        public TextMeshProUGUI textBox;
        public string prefix;
        public string suffix;

        [Space]
        public RegisterEvent DoUpdateText;
        public RegisterEvent DoReset;

        protected string resetText = "";

        protected virtual void Awake()
        {
            DoUpdateText.Register(this, "UpdateText");
            Reset();
        }

        protected virtual void OnDestroy()
        {
            DoUpdateText.Unregister();
        }

        protected virtual void OnEnable() {

        }

        protected virtual void OnDisable()
        {

        }

        protected virtual void UpdateText(params object[] objs)
        {
            try {
                UpdateText((string) objs[1]);
            }
            catch {
                try {
                    UpdateText((string) objs[0]);
                }
                catch {
                    UpdateText((objs[0] as StringObject).value);
                }
            }
        }

        public virtual void UpdateText(string msg)
        {
            textBox.text = prefix + msg + suffix;
        }

        protected void Reset(params object[] objs)
        {
            Reset();
        }

        public virtual void Reset()
        {
#if UNITY_EDITOR
            if (textBox == null) return;
#endif
            textBox.text = resetText;
        }

    }
}
