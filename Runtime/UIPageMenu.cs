﻿using PepUp.Events;

namespace PepUp.UI {
    [UnityEngine.AddComponentMenu("PepUp/UI/Page Menu")]
    public class UIPageMenu : Menu {

        public EventObject OnOpen;
        public EventObject OnClose;

        public event EventAction OnOpenEvent;
        public event EventAction OnCloseEvent;

        public override MenuTypes GetMenuType() {
            return MenuTypes.Page;
        }

        public override void Open(Menu parent) {
            if (isActive) return;

            if (parent != null) {
                parent.Close();
            }
            this.parent = null;
            this.Activate(true);
            OnOpen?.Invoke(gameObject.name);
            OnOpenEvent?.Invoke(gameObject.name);
        }

        public override Menu OpenNew(Menu parent) {
            parent.Open(this);
            return parent;
        }

        public override void Close() {
            base.Close();
            OnClose?.Invoke(gameObject.name);
            OnCloseEvent?.Invoke(gameObject.name);
        }
    }
}