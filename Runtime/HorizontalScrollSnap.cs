﻿using UnityEngine;
using PepUp.Events;
using PepUp.Utility;
using UnityEngine.EventSystems;

namespace PepUp.UI {
    public class HorizontalScrollSnap : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public bool interactable = true;
        public RectTransform scrollPanel;
        public RectTransform center;
        public int itemDistance;
        public int scrollMinDistance;
        public bool loopable;
        public int maxItemCount = -1;

        [Space]
        public bool scaleByDistance;
        public Vector3 defaultScale = Vector3.one;
        public Vector3 activeScale = Vector3.one;

        [Space]
        public bool saveIndex = true;
        public string saveFileName = "ItemUI";

        //[Space]
        //public Color defaultColor = Color.white;
        //public Color activeColor = Color.white;

        [Space]
        public HorizontalScrollContent[] items;

        [Space]
        public RegisterEvent DoActive;
        public RegisterEvent DoDeactive;

        public event EventAction OnActive;
        public event EventAction OnDeactive;
        public event EventAction OnUpdate;

        public int currItemIndex { get; private set; }
        public int lastItemIndex { get; private set; }
        public int currIndex { get; private set; }

        protected float[] distances;
        protected bool dragging;

        private Vector2 _newPosition;

        protected virtual void Awake() {
            LoadIndex();

            int len = items.Length;
            if (maxItemCount < 0) maxItemCount = len;
            distances = new float[len];
            UpdateScrollRect();
            lastItemIndex = currItemIndex;

            DoActive.Register(this, "Active");
            DoDeactive.Register(this, "Deactive");

            enabled = false;
        }

        protected void OnDestroy()
        {
            DoActive.Unregister();
            DoDeactive.Unregister();
        }

        protected void Active(params object[] objs) {
            Active();
        }

        public void Active() {
            enabled = true;
        }

        protected void Deactive(params object[] objs)
        {
            Deactive();
        }

        public void Deactive()
        {
            enabled = false;
        }

        public void OnPointerDown(PointerEventData eventData) {
            if (!interactable) return;
            StartDrag();
        }

        public void OnPointerUp(PointerEventData eventData) {
            if (!interactable) return;
            EndDrag();
        }

        public void StartDrag()
        {
            enabled = dragging = true;
        }

        public void EndDrag()
        {
            dragging = false;
        }

        protected void Update() {
            if (dragging)
            {
                UpdateScrollRect();
            }
            else
            {
                lastItemIndex = currItemIndex;
                UpdateItems(currItemIndex);
            }
        }

        public virtual void LoadIndex() {
            int index = LoadFromSave();
            _newPosition = scrollPanel.anchoredPosition;
            _newPosition.x = index * itemDistance * -1;
            scrollPanel.anchoredPosition = _newPosition;

            if (!loopable) return;

            int len = items.Length;
            for (int i = 0; i < len; i++) {
                _newPosition = items[i].rectTransform.localPosition;
                _newPosition.x += itemDistance * index;
                items[i].rectTransform.localPosition = _newPosition;
            }
        }

        protected virtual int LoadFromSave() {
            SaveUtility.LoadFromBinaryFormatter(saveFileName, out int index);
            return index;
        }

        public virtual void SaveIndex() {
            if (!saveIndex) return;
            items[currItemIndex].SaveItem(saveFileName);
        }

        public void UpdateItems(int index) {
            currItemIndex = index;
            int len = items.Length;
            for (int i = 0; i < len; i++) {
                SetItemScale(i, currItemIndex);
                //SetItemColor(i, minItemIndex);
                UpdateItemIndex(i);
            }
            LerpToItem(items[currItemIndex].rectTransform);
        }

        public void UpdateScrollRect() {
            int len = items.Length;

            float secondMinDistance = -1;
            int secondMinIndex = 0;

            float minDistance = -1;
            int minIndex = 0;

            for (int i = 0; i < len; i++) {
                float difference = items[i].transform.position.x - center.transform.position.x;
                float distance = Mathf.Abs(difference);
                if (distance <= minDistance || minDistance < 0) {
                    if (minDistance >= 0) {
                        secondMinDistance = minDistance;
                        secondMinIndex = minIndex;
                    }
                    minDistance = distance;
                    minIndex = i;
                } else if (distance <= secondMinDistance || secondMinDistance < 0) {
                    secondMinDistance = distance;
                    secondMinIndex = i;
                }
                distances[i] = distance;

                int index = (int) items[i].rectTransform.localPosition.x / itemDistance;
                if ((loopable || maxItemCount != len) && distance >= itemDistance * (len - 2)) {
                    float distanceGap = items[i].transform.localPosition.x;
                    distanceGap += difference < 0 ? itemDistance * len : itemDistance * len * -1;
                    index = (int) distanceGap / itemDistance;

                    if ((index >= 0 && index < maxItemCount) || loopable) {
                        _newPosition.x = distanceGap;
                        _newPosition.y = items[i].transform.localPosition.y;
                        items[i].transform.localPosition = _newPosition;
                    }
                }
                index %= maxItemCount;
                if (index < 0) {
                    index = maxItemCount + index;
                }
                currIndex = index;
                items[i].UpdateContent(index, distance);
                OnUpdate?.Invoke(items[i]);
            }
            //if (minDistance > scrollMinDistance && minDistance < secondMinDistance && lastItemIndex == minIndex) {
            //    currItemIndex = secondMinIndex;
            //} else {
                currItemIndex = minIndex;
            //}
            //float minDistance = Mathf.Min(distances);

            //for (int i = 0; i < len; i++) {
            //    if (minDistance >= distances[i]) {
            //        minItemIndex = i;
            //    }
            //}
            for (int i = 0; i < len; i++) {
                SetItemState(i, currItemIndex);
                SetItemScale(i, currItemIndex);
                //SetItemColor(i, minItemIndex);
            }
        }

        //protected void SetItemColor(int i, int index) {
        //    Color currentColor = items[i].image.color;
        //    if (i == index) {
        //        currentColor = Color.Lerp(currentColor, activeColor, Time.deltaTime * 10f);
        //    } else if (currentColor != defaultColor) {
        //        currentColor = Color.Lerp(currentColor, defaultColor, Time.deltaTime * 10f);
        //    }
        //    items[i].image.color = currentColor;
        //}

        protected void SetItemScale(int i, int index) {
            if (scaleByDistance) {
                SetItemScaleByDistance(i);
                return;
            }
                
            if (i == index) {
                items[i].transform.localScale = Vector3.Lerp(items[i].transform.localScale, activeScale, Time.deltaTime * 10f);
            } else if (items[i].transform.localScale != defaultScale) {
                items[i].transform.localScale = Vector3.Lerp(items[i].transform.localScale, defaultScale, Time.deltaTime * 10f);
            }
        }

        protected void SetItemScaleByDistance(int index) {
            var item = items[index];
            float distance = Mathf.Abs(item.transform.position.x - center.transform.position.x);
            float distPercentage = distance / itemDistance;
            if (distPercentage > 1) {
                item.rectTransform.localScale = defaultScale;
                return;
            }
            Vector3 scaleValue = (distPercentage * (defaultScale - activeScale)) + activeScale;
            item.rectTransform.localScale = scaleValue;
        }

        protected void SetItemState(int i, int index) {
            if (i == index) {
                ActiveItemState(items[i]);
            } else {
                DeactiveItemState(items[i]);
            }
        }

        protected virtual void ActiveItemState(HorizontalScrollContent item) {
            item.Active();
            OnActive?.Invoke(item);
        }

        protected virtual void DeactiveItemState(HorizontalScrollContent item) {
            item.Deactive();
            OnDeactive?.Invoke(item);
        }

        protected void LerpToItem(RectTransform itemRect) {
            float targetPos = itemRect.localPosition.x * -1;
            float newX = Mathf.Lerp(scrollPanel.anchoredPosition.x, targetPos, Time.deltaTime * 5f);

            _newPosition.x = newX;
            _newPosition.y = scrollPanel.anchoredPosition.y;
            scrollPanel.anchoredPosition = _newPosition;

            if (Mathf.Abs(targetPos - scrollPanel.anchoredPosition.x) <= 0.01f) {
                _newPosition.x = targetPos;
                scrollPanel.anchoredPosition = _newPosition;
                enabled = false;
            }
        }

        protected void UpdateItemIndex(int i) {
            int index = (int) items[i].rectTransform.localPosition.x / itemDistance % maxItemCount;
            if (index < 0) {
                index = maxItemCount + index;
            }
            items[i].UpdateContent(index, distances[i]);
            OnUpdate?.Invoke(items[i]);
        }

#if UNITY_EDITOR
        protected void OnValidate() {
            if (Application.isPlaying) return;
            int value = itemDistance / 2;
            if (scrollMinDistance > value) {
                scrollMinDistance = value;
            }
            if (scrollMinDistance < 0) {
                scrollMinDistance = 0;
            }
            if (itemDistance < 0) {
                itemDistance = 0;
            }
        }
#endif
    }
}